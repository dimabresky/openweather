<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Openweather",
	"DESCRIPTION" => "Виджет погоды Openweather",
	"SORT" => 20,
	"CACHE_PATH" => "Y",
	"PATH" => array(
                    "ID" => "content"	
	),
);
