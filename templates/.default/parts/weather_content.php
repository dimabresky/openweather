<?php if (!empty($arResult["WEATHER_DATA"])): ?>
    <div class="openweather__temperature">
        <strong><?= $arResult["WEATHER_DATA"]["TEMP"] ?> &deg;С</strong>
    </div>
    <div class="openweather__icon">
        <img src="http://openweathermap.org/img/wn/<?= $arResult["WEATHER_DATA"]["ICON"] ?>@2x.png" alt="weather-icon">
    </div>
    <div class="openweather__description">
        <strong><?= $arResult["WEATHER_DATA"]["CONDITION"] ?></strong><br>
        <?= GetMessage('OPENWEATHER_WIND_TITLE') ?>: <strong><?= $arResult["WEATHER_DATA"]["WIND"] ?> <?= GetMessage('OPENWEATHER_WIND_MESURE_TITLE') ?></strong><br>
        <?= GetMessage('OPENWEATHER_HUMIDITY_TITLE') ?>: <strong><?= $arResult["WEATHER_DATA"]["HUMIDITY"] ?>%</strong>
    </div>
    <div class="clear"></div>
<?php endif; ?>
